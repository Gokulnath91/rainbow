package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import testng.api.base.Annotations;

public class AddRolePage extends Annotations {

	public AddRolePage enterRoleName(String data) {
		WebElement roleName = locateElement("Role-Name");
		clearAndType(roleName, data);
		return this;
	}

	public AddRolePage selectAllFeatrue() {
		WebElement createButton = locateElement("Role-Create");
		int y = createButton.getLocation().getY();
		((JavascriptExecutor) driver).executeScript("scroll(0,"+y+");");
		WebElement selectAll = locateElement("xpath", "//input[@value='SelectAll']/following-sibling::span[1]");
		waitUntilVisibility(selectAll);
		click(selectAll);
		return this;
	}

	public AddRolePage clickOnCreateRoleButton() {
		WebElement createButton = locateElement("Role-Create");
		click(createButton);
		return this;
	}
	
	public RolePage verifyMessage( String data) throws InterruptedException {
		WebElement errMsg = locateElement
				("xpath", "//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']//span[1]");
		waitUntilVisibility(errMsg);
		verifyExactText(errMsg, data);
		return new RolePage();
	}
	

}
