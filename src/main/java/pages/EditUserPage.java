package pages;

import org.openqa.selenium.WebElement;

import testng.api.base.Annotations;

public class EditUserPage extends Annotations {
	
	public EditUserPage getEmail() {
		WebElement email = locateElement("txtEmail");
		randomNumber=getAttributeValue(email);
		return this;
	}
	
	public EditUserPage clickOnInActiveButton() {
		WebElement InActive = locateElement("xpath","//input[@id='IsActive_checkUserEdit_click']/following-sibling::span[1]");
		click(InActive);
		return this;
		
	}
	
	public EditUserPage clickOnUpdateUserButton() {
		WebElement updateButton = locateElement("btnCreateUser");
		click(updateButton);
		return this;
	}
	
	public UserPage verifyMessage(String data) {
		WebElement errMsg = locateElement
				("xpath", "//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']//span[1]");
		waitUntilVisibility(errMsg);
		verifyExactText(errMsg, data);
		return new UserPage();
	}
	

}
