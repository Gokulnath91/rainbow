package pages;

import org.openqa.selenium.WebElement;

import testng.api.base.Annotations;

public class EditEntityPage extends Annotations {
	
	public EditEntityPage clickOnActiveButton() {
		WebElement edit = locateElement("xpath","//input[@id='IsActive_checkEntityEdit_click']/following-sibling::span[1]");
		click(edit);
		return this;

	}
	
	public EditEntityPage getIdentifier() {
		WebElement identifier = locateElement("txtCIKNumber");
		randomNumber=getAttributeValue(identifier);
		return this;
	}
	
	public EditEntityPage clickOnUpdateEntityButton() {
		WebElement edit = locateElement("btnCreateEntity");
		click(edit);
		return this;

	}
	
	public EntityPage verifyMessage(String data) {
		WebElement errMsg = locateElement
				("xpath", "//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']//span[1]");
		waitUntilVisibility(errMsg);
		verifyExactText(errMsg, data);
		return new EntityPage();
	}
	
	

	

}
