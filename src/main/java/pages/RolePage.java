package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import testng.api.base.Annotations;

public class RolePage extends Annotations {
	
	public AddRolePage clickOnAddRoleButton() {
		WebElement addRole = locateElement("btnAddRole");
		click(addRole);
		return new AddRolePage();
	}
	
	public RolePage enterRoleNameFromExcel(String data) {
		WebElement roleName = locateElement("fSearchName");
		clearAndType(roleName, data);
		return this;		
	}
	
	public RolePage clickOnSearcButton() {
		WebElement search = locateElement("btnSearchRole");
		click(search);
		return this;
		
	}
	
	public RolePage verifyCreatedRoleName(String data) {
		WebElement resultTable = locateElement("role-tbody");
		List<WebElement> resultList = resultTable.findElements(By.tagName("tr"));
		WebElement roleName = locateElement("xpath", "//tbody[@id='role-tbody']//td[1]");
		if (resultList.size() == 1 && getElementText(roleName).equals(data)) {
			reportStep("Role created successfully.", "pass");
		} else
			reportStep("Role not created successfully.", "fail");
		return this;
	}
	
	public EditRolePage clickOnEditButton() {
		WebElement editButton = locateElement("xpath","//tbody[@id='role-tbody']//a[1]");
		click(editButton);
		return new EditRolePage();
	}
	
	public RolePage verifyInactiveRoleName() {
		WebElement result = locateElement("page-selection");
	if(result.isDisplayed()) {
		reportStep("Role not InActivated", "fail");
	} else reportStep("Role InActivated successfully", "pass");
		return this;
	}
	
	

}
