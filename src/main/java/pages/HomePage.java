package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import testng.api.base.Annotations;

public class HomePage extends Annotations {
	
	public HomePage moveToProfile() throws InterruptedException {
		WebElement profileMenu = locateElement("xpath", "(//div[@class='dropdown']//img)[1]");
		moveToElement(profileMenu);
		return this;
	}
	
	public HomePage verifyLoginId(String data) {
		
		WebElement userID = locateElement("userId");
		waitUntilVisibility(userID);
		verifyExactText(userID, data);
		return this;
		
	}

	public LoginPage clickOnSignOut() {
		WebElement signOut = locateElement("partiallink", "Sign out");
		click(signOut);
		return new LoginPage();
	}
	
	public HomePage moveToAdmin()  {
		WebElement adminMenu = locateElement("xpath", "//label[contains(text(),'Admin')]");
		moveToElement(adminMenu);
		return this;
	}
	
	public EntityPage clickOnEntity() {
		WebElement Entity = locateElement("xpath", "//span[contains(text(),'Entity')]");
		click(Entity);
		return new EntityPage();
	}
	
	public UserPage clickOnUser() {
		WebElement User = locateElement("xpath", "//span[contains(text(),'User')]");
		click(User);
		return new UserPage();
	}
	
	public RolePage clickOnRole() {
		WebElement Role = locateElement("xpath", "//span[contains(text(),'Role')]");
		click(Role);
		return new RolePage();
	}
	
}
