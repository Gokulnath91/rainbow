package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import testng.api.base.Annotations;

public class UserPage extends Annotations {

	public AddUserPage clickOnAddUserButton() {
		WebElement addButton = locateElement("btnAddUser");
		click(addButton);
		return new AddUserPage();
	}

	public UserPage enterEmailIdFromData(String data) {
		WebElement email = locateElement("fEmail");
		String[] split = data.split("@");
		data = split[0] + randomNumber + "@" + split[1];
		clearAndType(email, data);
		return this;
	}
	
	public UserPage clearUserName() {
		WebElement email = locateElement("fSearchName");
		clear(email);
		return this;
	}
	
	public UserPage enterEmailId() {
		WebElement email = locateElement("fEmail");
		clearAndType(email, randomNumber);
		return this;
	}

	public UserPage clickOnSearch() {
		WebElement search = locateElement("btnSearchUser");
		click(search);
		return this;
	}

	public UserPage verifyCreatedUserEmail(String data) {
		WebElement resultTable = locateElement("user-tbody");
		List<WebElement> resultList = resultTable.findElements(By.tagName("tr"));
		WebElement CIK = locateElement("xpath", "//tbody[@id='user-tbody']//td[4]");
		String[] split = data.split("@");
		data = split[0] + randomNumber + "@" + split[1];
		if (resultList.size() == 1 && getElementText(CIK).equals(data)) {
			reportStep("User created successfully.", "pass");
		} else
			reportStep("User not created successfully.", "fail");
		return new UserPage();
	}

	public UserPage enterUserName(String data) {
		WebElement userName = locateElement("fSearchName");
		clearAndType(userName, data);
		return this;
	}

	public EditUserPage clickOnEditUser() {
		WebElement edit = locateElement("xpath", "(// a[contains(@class,'UserEditLink btn-xs pad0')]//i)[1]");
		click(edit);
		return new EditUserPage();

	}
	
	public UserPage verifyInactiveIndertifier() {
		WebElement result = locateElement("page-selection");
	if(result.isDisplayed()) {
		reportStep("User not InActivated", "fail");
	} else reportStep("User InActivated successfully", "pass");
		return this;
	}

	
	

}
