package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import testng.api.base.Annotations;

public class LoginPage extends Annotations {
	
		public LoginPage enterEmailId(String data) {
			WebElement email = locateElement("txtEmail");
			clearAndType(email, data);
			return this;
		}
		
		public LoginPage enterPassword(String data) {
			WebElement password = locateElement("txtPassword");
			clearAndType(password, data);
			return this;
		}
		
		public HomePage clickOnLoginButton() {
			WebElement LoginButton = locateElement("btnLogin");
			click(LoginButton);
			try {
				/*WebElement OkButton = locateElement("xpath", "//button[text()='Ok']");
				click(OkButton);*/
				driver.findElementByXPath("//button[text()='Ok']").click();
			} catch (Exception e) {
			}
			return new HomePage();
		}
		
		public LoginPage clickOnLoginButtonWithInvalidCredential() {
			WebElement LoginButton = locateElement("btnLogin");
			click(LoginButton);
			return this;
		}
		
		public LoginPage verifyErrorMessage( String data) throws InterruptedException {
			WebElement errMsg = locateElement
					("xpath", "//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-error']//span[1]");
			waitUntilVisibility(errMsg);
			verifyExactText(errMsg, data);
			return this;
			
		}
		
		
		
		
		
		
		

}
