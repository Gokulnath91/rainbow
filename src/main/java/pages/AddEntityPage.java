package pages;

import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import testng.api.base.Annotations;

public class AddEntityPage extends Annotations {
	
	public AddEntityPage enterEntityName(String data) {
		
		WebElement entityName = locateElement("txtName");
		clearAndType(entityName, data+(" "+randomNumber));
		return this;
	}
		
	public AddEntityPage selectEntityType(String data) {
		WebElement entityType = locateElement("txtEntityType");
		selectDropDownUsingText(entityType, data);
		return this;
	}
	
	public AddEntityPage enterCIK() {
		
		WebElement CIK = locateElement("txtCIKNumber");
		clearAndType(CIK, generateRandomNumber());
		return this;
	}

	public AddEntityPage enterTradingSymbol(String data) {
		
		WebElement tradingSymbol = locateElement("txtTradingSymbol");
		clearAndType(tradingSymbol, data);
		return this;
	}
	
	public AddEntityPage enterWebite(String data) {
		
		WebElement Webite = locateElement("txtWebsite");
		clearAndType(Webite, data);
		return this;
	}
	
	public AddEntityPage selectCountry(String data) {
		WebElement Coutry = locateElement("CountryId");
		selectDropDownUsingText(Coutry, data);
		return this;
	}
	
	public AddEntityPage clickOnCreateEntityButton() {
		
		WebElement createEntityButton = locateElement("btnCreateEntity");
		click(createEntityButton);
		return this;
	}
	
	public EntityPage verifyMessage( String data) throws InterruptedException {
		WebElement errMsg = locateElement
				("xpath", "//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']//span[1]");
		waitUntilVisibility(errMsg);
		verifyExactText(errMsg, data);
		return new EntityPage();
	}
	

}
