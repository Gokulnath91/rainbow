package pages;

import org.openqa.selenium.WebElement;

import testng.api.base.Annotations;

public class EditRolePage extends Annotations{
	
	public EditRolePage clickOnInActiveButton() {
		WebElement inActiveButton = locateElement("xpath","//input[@id='IsActive_checkroleEdit_click']/following-sibling::span[1]");
		click(inActiveButton);
		return this;
	}
	
	public EditRolePage clickOnUpdateRoleButton() {
		WebElement updateButton = locateElement("Role-Create");
		click(updateButton);
		return this;
	}

	public RolePage verifyMessage(String data) {
		WebElement errMsg = locateElement
				("xpath", "//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']//span[1]");
		waitUntilVisibility(errMsg);
		verifyExactText(errMsg, data);
		return new RolePage();
	}
	
}
