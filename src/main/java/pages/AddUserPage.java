package pages;

import org.openqa.selenium.WebElement;

import testng.api.base.Annotations;

public class AddUserPage extends Annotations {

	public AddUserPage enterUserName(String data) {
		WebElement userName = locateElement("txtUserName");
		clearAndType(userName, data+" "+generateRandomNumber());
		return this;
	}
	
	public AddUserPage selectUserRole(String data) {
		WebElement userRole = locateElement("txtUserRole");
		selectDropDownUsingText(userRole, data);
		return this;
	}
	
	public AddUserPage enterEmailID(String data) {
		WebElement email = locateElement("txtEmail");
		String[] split = data.split("@");
		data=split[0]+randomNumber+"@"+split[1];
		clearAndType(email, data);
		return this;
	}
	
	public AddUserPage enterContactNumber() {
		WebElement contactNum = locateElement("txtContactNo");
		clearAndType(contactNum, randomNumber);
		return this;
	}
	
	public AddUserPage selectEntity() {
		WebElement entity = locateElement("xpath","((//table[@id='EntityList'])[2]//span)[1]");
		click(entity);
		return this;
	}
	
	public AddUserPage clickOnCreateUserButton() {
		WebElement submit = locateElement("btnCreateUser");
		click(submit);
		return this;
	}
	
	public UserPage verifyMessage(String data) {
		WebElement errMsg = locateElement
				("xpath", "//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']//span[1]");
		waitUntilVisibility(errMsg);
		verifyExactText(errMsg, data);
		return new UserPage();
	}
	
}
