package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import testng.api.base.Annotations;

public class EntityPage extends Annotations {

	public AddEntityPage clickOnAddEntityButton() {
		WebElement addButton = locateElement("btnAddEntity");
		click(addButton);
		return new AddEntityPage();
	}
	
	public EntityPage clearEntityName() {
		WebElement entityName = locateElement("fSearchName");
		clear(entityName);
		return this;
	}


	public EntityPage enterIdentifier() {
		WebElement indentifier = locateElement("fContactPerson");
		clearAndType(indentifier, randomNumber);
		return this;

	}

	public EntityPage clickOnSearch() {
		WebElement search = locateElement("btnSearchEntity");
		click(search);
		return this;
	}

	public HomePage verifyCreatedEntityidentifier() {
		WebElement resultTable = locateElement("entity-tbody");
		List<WebElement> resultList = resultTable.findElements(By.tagName("tr"));
		WebElement CIK = locateElement("xpath", "//tbody[@id='entity-tbody']//td[4]");
		if (resultList.size() == 1 && getElementText(CIK).equals(randomNumber)) {
			reportStep("Entity created successfully.", "pass");
		} else
			reportStep("Entity not created successfully.", "fail");
		return new HomePage();
	}

	public EntityPage enterEntityName(String data) {
		WebElement eName = locateElement("fSearchName");
		clearAndType(eName, data);
		return this;

	}

	public EntityPage enterWebsite(String data) {
		WebElement email = locateElement("fEmail");
		clearAndType(email, data);
		return this;

	}
	
	public EntityPage clickOnActiveButton() {
		WebElement edit = locateElement("fActive");
		click(edit);
		return this;

	}
	
	public EditEntityPage clickOnEditEntity() {
		WebElement edit = locateElement("xpath", "(//a[contains(@class,'EntityEditLink btn-xs')]//i)[1]");
		click(edit);
		return new EditEntityPage();

	}
	
	public EntityPage verifyInactiveIndertifier(String data) {
		WebElement result = locateElement("xpath","//tbody[@id='entity-tbody']//td[2]");
		verifyPartialText(result, data);
		return this;

		
	}
	
	

}
