package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import testng.api.base.Annotations;

public class TC0004_CreateUserWithAllField extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC0004_CreateUserWithAllField";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC0004_CreateUserWithAllField";
	}
	
	@Test(dataProvider="fetchData")	
	public void positiveLogin(String userID,String pwd,String name,String role,String email,String msg) throws InterruptedException {
	
	new LoginPage()
	.enterEmailId(userID)
	.enterPassword(pwd)
	.clickOnLoginButton()
	.moveToAdmin()
	.clickOnUser()
	.clickOnAddUserButton()
	.enterUserName(name)
	.selectUserRole(role)
	.enterEmailID(email)
	.enterContactNumber()
	.selectEntity()
	.clickOnCreateUserButton()
	.verifyMessage(msg)
	.enterEmailIdFromData(email)
	.clickOnSearch()
	.verifyCreatedUserEmail(email);
	
}

}
