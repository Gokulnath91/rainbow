package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import testng.api.base.Annotations;

public class TC0008_InActiveRole extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC0008_InActiveRole";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC0008_InActiveRole";
	}
	
	@Test(dataProvider="fetchData")	
	public void positiveLogin(String userID,String pwd,String name,String msg)  {
	
	new LoginPage()
	.enterEmailId(userID)
	.enterPassword(pwd)
	.clickOnLoginButton()
	.moveToAdmin()
	.clickOnRole()
	.enterRoleNameFromExcel(name)
	.clickOnSearcButton()
	.clickOnEditButton()
	.clickOnInActiveButton()
	.clickOnUpdateRoleButton()
	.verifyMessage(msg)
	.enterRoleNameFromExcel(name)
	.clickOnSearcButton()
	.verifyInactiveRoleName();
	
	
	}

}
