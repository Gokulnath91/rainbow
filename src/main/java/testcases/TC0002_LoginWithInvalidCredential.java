package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import testng.api.base.Annotations;

public class TC0002_LoginWithInvalidCredential extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC0002_LoginWithInvalidCredential";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC0002_LoginWithInvalidCredential";
	}
	
	@Test(dataProvider="fetchData")	
	public void positiveLogin(String userID,String pwd,String msg) throws InterruptedException {
	
	new LoginPage()
	.enterEmailId(userID)
	.enterPassword(pwd)
	.clickOnLoginButtonWithInvalidCredential()
	.verifyErrorMessage(msg);
	
}

}
