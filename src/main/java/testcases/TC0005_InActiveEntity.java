package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import testng.api.base.Annotations;

public class TC0005_InActiveEntity extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC0005_InActiveEntity";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC0005_InActiveEntity";
	}
	
	@Test(dataProvider="fetchData")	
	public void positiveLogin(String userID,String pwd,String name,String msg,String msg1)  {
	
	new LoginPage()
	.enterEmailId(userID)
	.enterPassword(pwd)
	.clickOnLoginButton()
	.moveToAdmin()
	.clickOnEntity()
	.enterEntityName(name)
	.clickOnSearch()
	.clickOnEditEntity()
	.getIdentifier()
	.clickOnActiveButton()
	.clickOnUpdateEntityButton()
	.verifyMessage(msg)
	.clearEntityName()
	.enterIdentifier()
	.clickOnSearch()
	.verifyInactiveIndertifier(msg1);
}

}
