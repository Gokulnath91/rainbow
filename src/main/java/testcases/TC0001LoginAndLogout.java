package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import testng.api.base.Annotations;

public class TC0001LoginAndLogout extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC0001_LoginAndLogout";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC0001_LoginAndLogout";
	}
	
	@Test(dataProvider="fetchData")	
	public void positiveLogin(String userID,String pwd) throws InterruptedException {
	
	new LoginPage()
	.enterEmailId(userID)
	.enterPassword(pwd)
	.clickOnLoginButton()
	.moveToProfile()
	.verifyLoginId(userID)
	.moveToProfile()
	.clickOnSignOut();
	
}

}
