package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import testng.api.base.Annotations;

public class TC0007_CreateRoleWithAllFeature extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC0007_CreateRoleWithAllFeature";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC0007_CreateRoleWithAllFeature";
	}
	
	@Test(dataProvider="fetchData")	
	public void positiveLogin(String userID,String pwd,String name,String msg) throws InterruptedException {
	
	new LoginPage()
	.enterEmailId(userID)
	.enterPassword(pwd)
	.clickOnLoginButton()
	.moveToAdmin()
	.clickOnRole()
	.clickOnAddRoleButton()
	.enterRoleName(name)
	.selectAllFeatrue()
	.clickOnCreateRoleButton()
	.verifyMessage(msg)
	.enterRoleNameFromExcel(name)
	.clickOnSearcButton()
	.verifyCreatedRoleName(name);
	
}

}
