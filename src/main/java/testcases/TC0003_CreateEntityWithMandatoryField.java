package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import testng.api.base.Annotations;

public class TC0003_CreateEntityWithMandatoryField extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC0003_CreateEntityWithMandatoryField";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC0003_CreateEntityWithMandatoryField";
	}
	
	@Test(dataProvider="fetchData")	
	public void positiveLogin(String userID,String pwd,String eName,String eType,String eIndentifier,String tSymbol,
			String webstie,String country,String msg) throws InterruptedException {
	
	new LoginPage()
	.enterEmailId(userID)
	.enterPassword(pwd)
	.clickOnLoginButton()
	.moveToAdmin()
	.clickOnEntity()
	.clickOnAddEntityButton()
	.selectEntityType(eType)
	.enterCIK()
	.enterEntityName(eName)
	.enterTradingSymbol(tSymbol)
	.enterWebite(webstie)
	.selectCountry(country)
	.clickOnCreateEntityButton()
	.verifyMessage(msg)
	.enterIdentifier()
	.clickOnSearch()
	.verifyCreatedEntityidentifier();
		
	
}

}
