package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import testng.api.base.Annotations;

public class TC0006_InActiveUser extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC0006_InActiveUser";
		testcaseDec = "Login into leaftaps";
		author = "Gokul";
		category = "smoke";
		excelFileName = "TC0006_InActiveUser";
	}
	
	@Test(dataProvider="fetchData")	
	public void positiveLogin(String userID,String pwd,String name,String msg)  {
	
	new LoginPage()
	.enterEmailId(userID)
	.enterPassword(pwd)
	.clickOnLoginButton()
	.moveToAdmin()
	.clickOnUser()
	.enterUserName(name)
	.clickOnSearch()
	.clickOnEditUser()
	.getEmail()
	.clickOnInActiveButton()
	.clickOnUpdateUserButton()
	.verifyMessage(msg)
	.clearUserName()
	.enterEmailId()
	.clickOnSearch()
	.verifyInactiveIndertifier();
	
	}

}
